﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MandelbrotSet
{
    [Serializable]
    public class ParamFractal
    {
        public int Num;
        public Bitmap Img;

        public Guid CodecGuid;
        public Int64 QualityImage;
        public Int32 Width;
        public Int32 Height;
        public Int32 StartWidth;
        public Int32 OriginalWidth;
        public Int32 OriginalHeight;
        public ParamFractal() { }

        public ParamFractal(SerializationInfo sInfo, StreamingContext contextArg)
        {
            this.Num = (int)sInfo.GetValue("Num", typeof(int));
            this.Img = (Bitmap)sInfo.GetValue("Img", typeof(Bitmap));
            this.Width = (Int32)sInfo.GetValue("Width", typeof(Int32));
            this.Width = (Int32)sInfo.GetValue("Height", typeof(Int32));
            this.CodecGuid = (Guid)sInfo.GetValue("CodecGuid", typeof(Guid));
            this.QualityImage = (Int64)sInfo.GetValue("QualityImage", typeof(Int64));
        }

        public void GetObjectData(SerializationInfo sInfo, StreamingContext contextArg)
        {
            sInfo.AddValue("Num", this.Num);
            sInfo.AddValue("Img", this.Img);
            sInfo.AddValue("Width", this.Width);
            sInfo.AddValue("Height", this.Height);
            sInfo.AddValue("CodecGuid", this.CodecGuid);
            sInfo.AddValue("QualityImage", this.QualityImage);
        }

        
    }
}
