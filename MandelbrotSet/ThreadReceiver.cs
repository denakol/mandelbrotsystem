﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MandelbrotSet
{
    public class ThreadReceiver
    {
        public static List<Int32> Threads; 

        private readonly TcpClient _tcpClient;

        private readonly AutoResetEvent _endEvent;
        private IProgress<StatusMessage> _progress;

        private byte[] _array;
        public  Boolean Error;
        public  ParamFractal CurrentBitmap;
        private readonly byte[] _arraySend;
        private readonly ManualResetEvent _startEvent;
        public ParamFractal OutputBitmap;
        public ThreadReceiver(){}

        public ThreadReceiver(TcpClient tcpClient, ParamFractal paramFractal, ManualResetEvent eEvent, AutoResetEvent end, IProgress<StatusMessage> progress)
        {
            _tcpClient = tcpClient;
            _endEvent = end;
            _arraySend = ObjectToByteArray(paramFractal);
            CurrentBitmap = paramFractal;
            _startEvent = eEvent;
            new Thread(Function).Start();
            Threads.Add(new Random().Next(100));
            _progress = progress;
        }



        /// <summary>
        /// принимаем данные от каждого клиента
        /// </summary>
        public void Function()
        {
            try
            {
                _startEvent.WaitOne();
                _progress.Report(new StatusMessage() { Message = "Отправка данных на" + _tcpClient.Client.RemoteEndPoint +" блока номер" + CurrentBitmap.Num});
                _tcpClient.GetStream().Write(_arraySend, 0, _arraySend.Length);

                var lengthArray = new byte[4];
                _tcpClient.GetStream().Read(lengthArray, 0, lengthArray.Length);
                var length = BitConverter.ToInt32(lengthArray, 0);
                _array = new byte[length];
                var readBytes = 0;
                _progress.Report(new StatusMessage() { Message = "Прием данных от" + _tcpClient.Client.RemoteEndPoint + " блока номер" + CurrentBitmap.Num });
                readBytes = _tcpClient.GetStream().Read(_array, 0, _array.Length);
                if (readBytes != length)
                {
                    while (readBytes != length)
                    {
                        readBytes += _tcpClient.GetStream().Read(_array, readBytes, length - readBytes);
                    }
                }
                _progress.Report(new StatusMessage() { Message = "Прием данных завершен от" + _tcpClient.Client.RemoteEndPoint + " блока номер" + CurrentBitmap.Num });
                _progress.Report(new StatusMessage() { Message = "Преобразование изображения: " + _tcpClient.Client.RemoteEndPoint + " блока номер" + CurrentBitmap.Num });
                OutputBitmap = ByteArrayToObject(_array);
                if (OutputBitmap.Img == null)
                {
                    throw new Exception();
                }
            }
            catch(Exception ex)
            {
                _progress.Report(new StatusMessage() { Message = "Произошла ошибка: " + _tcpClient.Client.RemoteEndPoint + " блока номер" + CurrentBitmap.Num + "блок будет отправлен другому клиенту"});
                Error = true;
            }
            

            lock (Threads)
            {
                Threads.Remove(Threads.Last());
                if (Threads.Count == 0) _endEvent.Set();
            }
        }
        private byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null) return null;
            var bf = new BinaryFormatter();
            var ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }

        private ParamFractal ByteArrayToObject(byte[] arrBytes)
        {
            using (MemoryStream stream = new MemoryStream(arrBytes))
            {
                // Загружаем картинку из потока
                var bitmap = new Bitmap(stream);
                return new ParamFractal() { Img = bitmap };
            }
        }

        /// <summary>
        /// вернуть полученные данные
        /// </summary>
        /// <returns></returns>
        public byte[] GetBytes()
        {
            return _array;
        }
    }
}
