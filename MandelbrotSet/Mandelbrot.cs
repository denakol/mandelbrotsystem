﻿using Microsoft.Drawing;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Numerics;
using System.Threading;

namespace MandelbrotSet
{
    class Mandelbrot
    {

        public Mandelbrot(int width, int height, int n)
        {
            _height = height;
            _width = width;
            _bmp = new Bitmap(_width, _height);
            _n = n;

            _rMin = -2.5;
            _iMin = -1.0;
            _rScale = (Math.Abs(_rMin) + Math.Abs(1.0)) / _width;
            _iScale = (Math.Abs(_iMin) + Math.Abs(1.0)) / _height;
            _mreStart = new ManualResetEvent(false);
            _mreEnd = new ManualResetEvent(false);

            _watch=new Stopwatch();
        }

        public Mandelbrot(ParamFractal numBitmap, int n)
        {
            _height = numBitmap.OriginalHeight;
            _width = numBitmap.Width;
            _startWidth = numBitmap.StartWidth;
            _bmp = new Bitmap(_width, _height);
            _n = n;

            _rMin = -2.5;
            _iMin = -1.0;
            _rScale = (Math.Abs(_rMin) + Math.Abs(1.0)) / numBitmap.OriginalWidth;
            _iScale = (Math.Abs(_iMin) + Math.Abs(1.0)) / numBitmap.OriginalHeight;
            _mreStart = new ManualResetEvent(false);
            _mreEnd = new ManualResetEvent(false);
        }

        public Bitmap Render(int maxNumberOfThread)
        {
            _palette = GenerateColorPalette();
            _currentNumberOfThread = maxNumberOfThread;
            ParallelRender(maxNumberOfThread);
            
            return _bmp;
        }


        private void ParallelRender(int currNumberOfThread)
        {
            using (FastBitmap fastBmp = new FastBitmap(_bmp))
            {
                int threadWidth = _width / currNumberOfThread;
                for (int i = 0; i < currNumberOfThread; i++)
                {
                    int startW = threadWidth * i;
                    int endW = threadWidth * i + threadWidth;
                    if (i == currNumberOfThread - 1)
                    {
                        endW = _width;
                    }
                    new Thread(() => DrawMandelbrot(startW, endW, fastBmp)).Start();
                }
                _mreStart.Set();
                _mreEnd.WaitOne();
                _mreEnd.Reset();
                _mreStart.Reset();
            }
        }

        private List<Color> GenerateColorPalette()
        {
            var palette = new List<Color>();
            for (int i = 0; i <= 255; i++)
            {
                palette.Add(Color.FromArgb(i,i, i, 255));
            }
            return palette;
        }

        private void DrawMandelbrot(int startWidth, int endWidth, FastBitmap fastBmp)
        {
            _mreStart.WaitOne();
            for (int x = startWidth; x < endWidth; x++)
            {
                for (int y = 0; y < _height; y++)
                {
                    var c = new Complex((x +_startWidth) * _rScale + _rMin, y * _iScale + _iMin);
                    Complex z = c;
                    for (int i = 0; i < _n; i++)
                    {
                        if (z.Magnitude >= 2.0)
                        {
                            fastBmp.SetColor(x, y, _palette[i]);
                            break;
                        }
                        z = c + Complex.Pow(z, 2);
                    }
                }
            }
            lock (_lock)
            {
                Interlocked.Decrement(ref _currentNumberOfThread);
                if (_currentNumberOfThread == 0)
                {
                    _mreEnd.Set();
                }
            }
        }

        private readonly Bitmap _bmp;
        private readonly int _width;
        private readonly int _height;
        private Int32 _startWidth;
        private readonly int _originalWidth;
        private readonly int _origainalHeight;
        private readonly int _n;

        private readonly double _rMin;
        private readonly double _iMin;
        private readonly double _rScale;
        private readonly double _iScale;
        private List<Color> _palette;

        private Stopwatch _watch;
        private int _currentNumberOfThread;
        private readonly ManualResetEvent _mreStart;
        private readonly ManualResetEvent _mreEnd;
        private object _lock= new object();
        private ParamFractal numBitmap;
    }
}
