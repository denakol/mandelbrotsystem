﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MandelbrotSet
{
    public class ServerMandelbrot
    {
        private String ip;
        private String _serverIp;
        private readonly Socket _client;
        private List<TcpClient> _clients = new List<TcpClient>();
        private IProgress<StatusMessage> _progress;
        private Client _clientCompute;

        public ServerMandelbrot(IProgress<StatusMessage> progress)
        {
            string myHost = Dns.GetHostName();
            _clientCompute = new Client(progress);
            _serverIp = Dns.GetHostByName(myHost).AddressList[0].ToString();
            _client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            _client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 5);
            _progress = progress;
        }
        public void AllowCompute()
        {
            Task.Run(() =>
            {
                _clientCompute.Start();
            });
        }
        /// <summary>
        /// запускаем сервер
        /// </summary>
        /// <param name="heightResult"></param>
        /// <param name="widthResult"></param>
        /// <param name="codecGuid"></param>
        /// <param name="quality"></param>
        /// <returns></returns>
        public async Task<Bitmap> Start(Int32 heightResult, Int32 widthResult, Guid codecGuid, Int64 quality)
        {
            Byte[] buffer = Encoding.Unicode.GetBytes(_serverIp);
            //отсылаем всем свой ip
            _client.SendTo(buffer, new IPEndPoint(IPAddress.Broadcast, 2222));
            _progress.Report(new StatusMessage() { Message = "Отправка всем приглашения: " + _serverIp });
            return await Task.Run(() =>
                       {
                           var tcpListener = new TcpListener(IPAddress.Any, 3333);
                           //ожидаем подключения клиентов
                           tcpListener.Start();
                           while (true)
                           {
                               //проверяем, есть ли запросы на подключение
                               if (tcpListener.Pending())
                               {
                                   _clients.Add(tcpListener.AcceptTcpClient());
                                   _progress.Report(new StatusMessage() { Message = "Подключен клиент: " + _clients[_clients.Count - 1].Client.RemoteEndPoint });
                               }
                               else
                               {

                                   Thread.Sleep(5000);
                                   if (!tcpListener.Pending())
                                   {
                                       break;
                                   }
                               }
                           }
                           tcpListener.Stop();
                           if (_clients.Count < 1)
                           {
                               _progress.Report(new StatusMessage() { Message = "Не подключилось ни одного клиента: " + _clients.Count });
                               return null;
                           }
                           _progress.Report(new StatusMessage() { Message = "Клиенты приняты в количестве: " + _clients.Count });

                           var outBitmap = new Bitmap(widthResult, heightResult);
                           var fractalParams = new List<ParamFractal>();
                           var width = (widthResult / _clients.Count);
                           var widthPerClient = (widthResult / _clients.Count);

                           for (int x = 0; x < widthResult; x += width)
                           {
                               fractalParams.Add(new ParamFractal()
                               {
                                   Num = fractalParams.Count,
                                   Width = widthPerClient,
                                   StartWidth = x,
                                   OriginalWidth = widthResult,
                                   OriginalHeight = heightResult,
                                   CodecGuid = codecGuid,
                                   QualityImage = quality
                               });
                           }

                           var start = new ManualResetEvent(false);
                           //сервачок ждет событие поэтому использем авто
                           var end = new AutoResetEvent(false);
                           var receiversList = new List<ThreadReceiver>();
                           ThreadReceiver.Threads = new List<int>();
                           for (int i = 0; i < fractalParams.Count; i++)
                           {
                               receiversList.Add(new ThreadReceiver(_clients[i], fractalParams[i], start, end, _progress));
                           }
                           _progress.Report(new StatusMessage() { Message = "Отправляем данные: " + _clients.Count });
                           start.Set();
                           end.WaitOne();
                           ConcurrentBag<ParamFractal> ErrorBlocks = new ConcurrentBag<ParamFractal>();
                           for (int i = 0; i < receiversList.Count; i++)
                           {
                               if (!receiversList[i].Error)
                               {
                                   fractalParams[i] = receiversList[i].OutputBitmap;
                               }
                               else
                               {
                                   _progress.Report(new StatusMessage() { Message = "Произошла ошибка, не высчитан блок: " + fractalParams[i].Num });
                                   ErrorBlocks.Add(fractalParams[i]);
                               }
                           }

                           if (ErrorBlocks.Count > 0)
                           {
                               _progress.Report(new StatusMessage() { Message = "Производим вычисление пропущенных блоков" });
                               try
                               {
                                   DistributeTasks(new Stack<ParamFractal>(ErrorBlocks), _clients, fractalParams);
                               }
                               catch (Exception ex)
                               {
                                   _progress.Report(new StatusMessage() { Message = "Произошла ошибка нет клиентов для обработки пропущенных блоков" });
                                   return null;
                               }

                           }

                           Int32 disconnected = 1;
                           foreach (var client in _clients)
                           {
                               client.GetStream().Write(BitConverter.GetBytes(disconnected), 0, 4);
                           }
                           _progress.Report(new StatusMessage() { Message = "Все отлично, отключаем клиентов" });
                           Task.Run(() =>
                           {

                               foreach (var client in _clients)
                               {
                                   try
                                   {
                                       client.Close();
                                   }
                                   catch (Exception ex)
                                   {
                                   }
                               }
                               _clients.Clear();

                           });
                           _progress.Report(new StatusMessage() { Message = "Склеиваем исходное изображение" });
                           fractalParams = fractalParams.OrderBy(x => x.Num).ToList();
                           var gr = Graphics.FromImage(outBitmap);
                           var pos = 0;
                           //клеим выходное изображение
                           foreach (ParamFractal t in fractalParams)
                           {
                               gr.DrawImage(t.Img, pos, 0);
                               pos += t.Img.Width;
                           }
                           _progress.Report(new StatusMessage() { Message = "Конец" });
                           return outBitmap;
                       });
        }

        private void DistributeTasks(Stack<ParamFractal> tasks, IEnumerable<TcpClient> clients, List<ParamFractal> bitmaps)
        {
            var start = new ManualResetEvent(false);
            var end = new AutoResetEvent(false);
            var receiversList = new List<ThreadReceiver>();
            ThreadReceiver.Threads = new List<int>();
            ConcurrentBag<ParamFractal> ErrorBlocks = new ConcurrentBag<ParamFractal>();
            _clients.RemoveAll(x => !x.Connected);

            if (_clients.Count > 0)
            {
                for (int i = 0; i < _clients.Count(); i++)
                {
                    if (i > tasks.Count)
                    {
                        break;
                    }
                    Int32 continueCompute = 2;
                    try
                    {
                        _clients[i].GetStream().Write(BitConverter.GetBytes(continueCompute), 0, 4);
                        receiversList.Add(new ThreadReceiver(_clients[i], tasks.Pop(), start, end, _progress));
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }

                start.Set();
                end.WaitOne();
                foreach (var item in receiversList)
                {
                    if (!item.Error)
                    {
                        bitmaps[item.CurrentBitmap.Num] = item.OutputBitmap;
                    }
                    else
                    {
                        _progress.Report(new StatusMessage() { Message = "Произошла ошибка, не высчитан блок: " + bitmaps[item.CurrentBitmap.Num] });
                        ErrorBlocks.Add(bitmaps[item.CurrentBitmap.Num]);
                    }
                }
                if (ErrorBlocks.Count > 0)
                {
                    DistributeTasks(new Stack<ParamFractal>(ErrorBlocks), clients, bitmaps);
                }
                if (tasks.Count > 0)
                {
                    DistributeTasks(tasks, clients, bitmaps);
                }
            }

            else
            {
                throw new Exception();
            }

        }
    }
}
