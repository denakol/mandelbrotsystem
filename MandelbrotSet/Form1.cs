﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Linq;
namespace MandelbrotSet
{
    public partial class MainForm : Form
    {
        private ServerMandelbrot server;
        private Boolean ClientRun = false;
        private readonly Progress<StatusMessage> _progress;


        public MainForm()
        {
            InitializeComponent();
            _progress = new Progress<StatusMessage>();
            _progress.ProgressChanged += Report;
            listBox1.DataSource = ImageCodecInfo.GetImageDecoders().Select(x =>x.CodecName.Split(new char[]{' '})[1]).ToList();
            server = new ServerMandelbrot(_progress);           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            server.AllowCompute();
            txtLog.Text += "Вы стали клиентом в сети";
            
        }
        public void Report(object sender, StatusMessage statusMessage)
        {
            txtLog.Text += statusMessage.Message + "\r\n";
        }


        private  async void button2_Click(object sender, EventArgs e)
        {
            var width = Convert.ToInt32(txt_width.Text);
            var height = Convert.ToInt32(txt_height.Text);
            txt.Width = width;
            txt.Height = height;
            var codecGui = ImageCodecInfo.GetImageDecoders().First(x => x.CodecName.Split(new char[] { ' ' })[1] == (String)listBox1.SelectedItem).FormatID;
            Int64 quality = 100L;
            txt.Image = await server.Start(height, width, codecGui, quality);         
        }
    }
}
