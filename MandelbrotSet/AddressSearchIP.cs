﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MandelbrotSet
{
    public class AddressSearchIP
    {
        /// <summary>
        /// посылает адрес в сеть
        /// </summary>
        private readonly Socket _client;

        /// <summary>
        /// принимает адреса из сети
        /// </summary>
        private readonly Socket _listener;

        /// <summary>
        /// адрес последнего найденного сервера
        /// </summary>
        private String _serverIp;

        /// <summary>
        /// флаг времени прослушивания
        /// </summary>
        private Boolean _flag = true;

        public String Ip { get { return _serverIp.Replace("\0", ""); } }

        public Boolean Flag { get { return _flag; } }

        public Socket Sender { get { return _client; } }

        public Socket Receiver { get { return _listener; } }
        public AddressSearchIP()
        {
            _listener = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            _listener.Bind(new IPEndPoint(IPAddress.Any, 2222));


            _client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            _client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);


            string myHost = Dns.GetHostName();
            _serverIp = Dns.GetHostByName(myHost).AddressList[0].ToString();
        }

        public void Start()
        {
            var iter = 0;
            while (_flag)
            {
                if (iter == 0)
                {
                    Byte[] buffer = Encoding.Unicode.GetBytes(_serverIp);
                    _client.SendTo(buffer, new IPEndPoint(IPAddress.Broadcast, 2222));
                    iter++;
                }

                Byte[] data = new byte[256];
                var rep = (EndPoint)new IPEndPoint(IPAddress.Broadcast, 2222);
                _listener.ReceiveFrom(data, ref rep);
                string ip = Encoding.Unicode.GetString(data);

                //ждем команды завершения прослушки
                if (ip.Replace("\0", "") == "go")
                {
                    _flag = false;
                    break;
                }

                _serverIp = ip.Replace("\0", "");
            }
        }
    }
}
