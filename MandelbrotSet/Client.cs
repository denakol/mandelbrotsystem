﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace MandelbrotSet
{
    public class Client
    {
        /// <summary>
        /// посылает адрес в сеть
        /// </summary>
        private TcpClient _tcpClient;
        private IProgress<StatusMessage> _progress;
        /// <summary>
        /// принимает адреса из сети
        /// </summary>
        private readonly Socket _listener;

        /// <summary>
        /// адрес последнего найденного сервера
        /// </summary>
        private String _serverIp;
        private String _ipServer;
        /// <summary>
        /// флаг времени прослушивания
        /// </summary>
        private Boolean _flag = true;

        public String Ip { get { return _serverIp.Replace("\0", ""); } }

        public Boolean Flag { get { return _flag; } }


        public Socket Receiver { get { return _listener; } }
        public Client(IProgress<StatusMessage> progress)
        {
            _listener = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            _listener.Bind(new IPEndPoint(IPAddress.Any, 2222));
            _progress = progress;
            string myHost = Dns.GetHostName();
            _serverIp = Dns.GetHostByName(myHost).AddressList[0].ToString();
        }

        public void Start()
        {
           
            while (_flag)
            {
                //массив данных от сервера(приглашение)
                Byte[] data = new byte[256];
                var rep = (EndPoint)new IPEndPoint(IPAddress.Broadcast, 2222);
                _listener.ReceiveFrom(data, ref rep);
                
                var ip = Encoding.Unicode.GetString(data).Split(new char[] { '\0' });
                _progress.Report(new StatusMessage() { Message = "Вы подключаетесь к серверу: " + ip[0]});
                try
                {
                    _tcpClient = new TcpClient();
                    _tcpClient.Connect(new IPEndPoint(IPAddress.Parse(ip[0]), 3333));
                    _progress.Report(new StatusMessage() { Message = "Вы подключены к серверу: " + ip[0] });
                    while (true)
                    {
                        //прием параметров от сервера
                        data = new byte[1048576];
                        _tcpClient.GetStream().Read(data, 0, data.Length);
                        _progress.Report(new StatusMessage() { Message = "Приняты данные на обработку: " + ip[0] });
                        var numBitmap = ByteArrayToObject(data) as ParamFractal;
                        var fractal = new Mandelbrot(numBitmap, 255);
                        numBitmap.Img = fractal.Render(Environment.ProcessorCount);

                        ImageCodecInfo encoder = ImageCodecInfo
                        .GetImageDecoders()
                        .First(codec => codec.FormatID == numBitmap.CodecGuid);
                        EncoderParameters myEncoderParameters = new EncoderParameters();
                        // Уровень сжатия
                        myEncoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, numBitmap.QualityImage);
                        
                        byte[] buffer;
                        // Сохраняем изображение в поток
                        using (MemoryStream stream = new MemoryStream())
                        {
                            numBitmap.Img.Save(stream, encoder, myEncoderParameters);
                            // Сохраняем поток в массив
                            buffer = stream.ToArray();
                        }
                        var lengthArray = BitConverter.GetBytes(buffer.Length);
                        _tcpClient.GetStream().Write(lengthArray, 0, lengthArray.Length);
                        _tcpClient.GetStream().Write(buffer, 0, buffer.Length);
                        _progress.Report(new StatusMessage() { Message = "Данные обработаны, отправка на: " + ip[0] });
                        //ждем команды завершения прослушки
                        _tcpClient.GetStream().Read(data, 0, 4);
                        if (BitConverter.ToInt32(data, 0) == 1)
                        {
                            _progress.Report(new StatusMessage() { Message = "Сервер не дал нового задания: " + ip[0] });
                            break;
                        }
                        _progress.Report(new StatusMessage() { Message = "У сервера остались блоки на обработку: " + ip[0] });
                    }
                }
                catch(Exception ex)
                {
                    _progress.Report(new StatusMessage() { Message = "Произошла ошибка: " +ex.Message + ip[0] });
                    if(_tcpClient!=null)
                    {
                        _tcpClient.Close();
                    }
                }
            }
        }

        private byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null) return null;
            var bf = new BinaryFormatter();
            var ms = new MemoryStream();
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }

        /// <summary>
        /// перевод массива байт в объект
        /// </summary>
        /// <param name="arrBytes"></param>
        /// <returns></returns>
        private Object ByteArrayToObject(byte[] arrBytes)
        {
            var memStream = new MemoryStream();
            var binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = (Object)binForm.Deserialize(memStream);
            return obj;
        }
    }
}
